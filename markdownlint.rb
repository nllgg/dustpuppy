#!/usr/bin/ruby

# https://github.com/markdownlint/markdownlint/blob/master/docs/RULES.md

all

rule 'MD013', :line_length => false

rule 'MD025', :level => false

rule 'MD026', :punctuation => ".,;!"
