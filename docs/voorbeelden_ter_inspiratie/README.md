# Voorbeelden ter inspiratie

We nodigen iedereen uit te beschrijven wat hij of zij al heeft gebouwd. Dit kan als inspiratie dienen voor ons project.

## Hoe bij te dragen

Voeg een link toe onder voorbeelden. Je kunt een enkelvoudig [(markdown!)](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md) document maken waarin je beschrijft wat je hebt, of een directory met meerdere documenten en foto's. Als je hardware beschrijft is het handig om zoveel mogelijk links te plaatsen naar de specificatie pagina van de fabrikant.

Map- en bestandsnamen schrijven we in snakecase, zonder hoofdletters.

## Voorbeelden

- [Server met 14 harddisks, CentOS, 500W voeding](14hdd_centos_500w.md)
