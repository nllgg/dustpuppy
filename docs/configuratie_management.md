# Configuratie management

## In grote lijnen

Er komt een Gitlab repository met daarin de code om een systeem in een bepaalde staat te krijgen. Deze code bevat **geen** persoonlijke data zoals gebruikersnamen en wachtwoorden of bijvoorbeeld de hostname van de dustpuppy server.
Er moet door de eigenaar een configuratiebestand worden gemaakt (template komt ook ergens op Gitlab te staan) en dat moet door de gebruiker op de server worden geplaatst. Dit bestand bevat alle geheime data. In het geval van Puppet kan worden gekozen voor [Hiera e-yaml](https://github.com/voxpupuli/hiera-eyaml).
Direct na de installatie van het operatingsystem wordt de repository uitgecheckt en de code die daarin staat uitgevoerd.

## Configuratie management repository

Bevat een instructie over hoe te gebruiken en een aantal modules. Het idee is dat een gebruiker zelf kan kiezen welke modules hij wil gebruiken door gebruik te maken van rollen en profielen. Wil de gebruiker een Nginx webserver met Ruby/Java/Php/Python applicatieserver en MySQL backend dan kan hij de `webserver` rol toevoegen aan zijn dustpuppy. Die rol bevat vervolgens de profielen `Nginx` en `Mysql` en een of meerdere van de eerder genoemde programmeertalen.
Alle variabelen hebben een standaardinstelling die (op z'n minst redelijk) werkt. Ze zijn te vervangen met waarden uit het eigen configuratiebestand.
Iedereen kan bijdragen aan de werking van de modules door een Merge Request te openen.

## Eigen configuratiebestand

Er komt een template met standaardinstellingen die werken voor dustpuppy. Dit bestand kan worden gedownload en dan worden aangepast door de gebruiker. Daarna wordt het bestand (of in het geval van bijvoorbeeld Hiera e-yaml) naar dustpuppet geupload om daar door de configuratiemanagement software te worden gebruikt.

## Eigen configuratiebestand in een online service

Een eigen configuratie bestand maken en bijhouden kost tijd en wijzigingen bijhouden in de opmaak van het bestand vormt een risico voor de stabiliteit. Veel handiger zou het zijn als we zorgen dat we alle geheime gegevens in een online password service opslaan en dat het configuratiemanagement systeem de API key heeft om die gegevens op te halen. Je kunt dat de gehele bootstrap automatiseren door tijdens de setup die API key mee te geven. Ik weet dat er voor [Puppet een Lastpass module](https://forge.puppet.com/MiamiOH/lastpass) bestaat (die alleen Centos 7 suppport momenteel) maar er bestaan meer systemen die een integratie met Ansible en Puppet mogelijk maken. Voorbeeld is [Device42](https://www.device42.com/features/). Het mooie van zulke systemen is dat we een goede scheiding kunnen aanbrengen tussen code die configuratie toepast en de gegevens die ingesteld worden. Je hoeft dan na de installatie van de server in wezen nooit meer op het ding in te loggen.
