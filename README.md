# Welkom bij het Dustpuppy project

De documenten van dit project zijn mooi te lezen op: [nllgg.gitlab.io/dustpuppy](https://nllgg.gitlab.io/dustpuppy).

## Docsify

Deze pagina's zijn gemaakt met [docsify](https://docsify.js.org)

Als je lokaal de webserver wil testen doe je dat met:

```shell
npm i
npm run dev
```

## Markdown in deze repository

We schrijven de documenten in deze repo in [(Gitlab flavoured) markdown](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md) omdat het heel eenvoudig te schrijven is en mooi leesbare pagina's oplevert en gemakkelijk omgezet kan worden naar een mooi leesbare pagina met Docsify (Zie hierboven).
Markdown moet voldoen aan tal van regels. We hebben [markdownlint](https://github.com/markdownlint/markdownlint) [aangezet](https://gitlab.com/nllgg/dustpuppy/blob/master/.gitlab-ci.yml#L7-12) in de CI van deze repository.
Markdownlint controleert of de documenten voldoen aan de regels van Markdown. We hebben een paar dingen [uitgezonderd](https://gitlab.com/nllgg/dustpuppy/blob/master/markdownlint.rb) van de controles.
Het is sterk aan te bevelen om Markdownlint op je laptop te [installeren](https://github.com/markdownlint/markdownlint#installation) en even [te draaien](https://github.com/markdownlint/markdownlint#usage) voor je iets commit.

---

[nllgg.nl](https://nllgg.nl)
